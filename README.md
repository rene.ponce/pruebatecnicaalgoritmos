# Sección de algoritmos

Los algoritmos están desarrollados en **JavaScript** para poder visualizar los resultados abrir los archivos **index.html** en un navegador y posteriormente abrir las **herramientas de desarrollador** y seleccionar la opción de **consola**


# Sección de preguntas

**Pregunta 1**
Utilizaría el patrón **Strategy** ya que este patrón permite tener diferentes métodos o algoritmos y de esta forma un objeto puede elegir método que necesite e intercambiarlos dinámicamente según lo vaya necesitando.

**Pregunta 2**
El patrón **Abstract Factory** crea diferentes objetos pero todos deben pertenecer a una misma familia, un caso de uso sería un sistema para bibliotecas.

El patrón **Factory** tiene una clase principal y puede tener métodos definidos y métodos abstractos y permite que las subclases alteren el tipo de objeto que se crea, un caso de uso puede ser una aplicación de gestión logística