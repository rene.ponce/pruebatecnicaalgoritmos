function contarPalabras(texto) {
  let obj = {};
  texto = replaceAll(texto, ['.', ',', ':', '—'], '');
  texto = texto.split(/\s+/).map(function(data){ 
    !obj[data] && (obj[data] = 0); 
    obj[data]++; 
  });
  return function(pos){
    return {"repeticiones": obj}[pos]
  }
}

function replaceAll( text, busca, reemplaza ){
  for (let i = 0; i <= busca.length; i++ ) {
    while (text.toString().indexOf(busca[i]) != -1)
      text = text.toString().replace(busca[i],reemplaza);
  }
  return text;
}

let texto = "Érase una vez una niñita que lucía una hermosa capa de color rojo. Como la niña la usaba muy a menudo, todos la llamaban Caperucita Roja. Un día, la mamá de Caperucita Roja la llamó y le dijo: —Abuelita no se siente muy bien, he horneado unas galletitas y quiero que tú se las lleves. —Claro que sí —respondió Caperucita Roja, poniéndose su capa y llenando su canasta de galletas recién horneadas. Antes de salir, su mamá le dijo: — Escúchame muy bien, quédate en el camino y nunca hables con extraños. —Yo sé mamá —respondió Caperucita Roja y salió inmediatamente hacia la casa de la abuelita.";
let result = contarPalabras(texto);

console.log(result("repeticiones"));